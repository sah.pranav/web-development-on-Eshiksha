<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">

	body {
  font-family: calibri;
  margin:0px;
  padding: 0px;
  background-color: #c2c7cf;
}
div
{
  margin: 0px;
  padding: 0px;
}
.navbar {
  overflow: hidden;
  background-color: #333;
  top:0px;
}
.navbar a {
  float: left;
  font-size: 18px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
.dropdown {
  float: left;
  overflow: hidden;
}
.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: #999;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
</head>
<head>
  <link rel="stylesheet" type="text/css" href="style/style1.css">
</head> 
<body bgcolor="#c8cac9">
	<img src="images/logo1.jpg" width="100%">
<div class="navbar">
  <a href="userprofile.php">Home</a>
  <?php
        include "db.php"; 
        $sql="SELECT lan FROM language";
        $res=$con->query($sql);
        if($res->num_rows>0)
        {
        while($row=$res->fetch_assoc())
        {
        ?>
        <a href="lan.php?lan=<?php echo $row['lan']; ?>"><?php echo $row['lan'];?></a>
      <?php 
      }
      }
  ?>
  <a href="postqueries.php">Post Queries</a>
  <a href="test.php">Take Quiz</a>
  <a href="certificate.php">Certification</a>
  <a href="logout.php">Logout</a>

</div>
</body>
</html>