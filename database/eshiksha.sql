-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2021 at 02:09 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshiksha`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `lan` varchar(100) NOT NULL,
  `approvel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`id`, `name`, `email`, `lan`, `approvel`) VALUES
(1, 'SRUJANA', 'srujana@gmail.com', 'JAVA', 'accepted'),
(2, 'srujana', 'srujana@gmail.com', 'php', 'rejected');

-- --------------------------------------------------------

--
-- Table structure for table `java`
--

CREATE TABLE `java` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `data` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `java`
--

INSERT INTO `java` (`id`, `title`, `data`) VALUES
(1, 'Introduction to java', '<p>Author : James Gosling </p>\r\n<p>Vendor : Sun Micro System(which has since merged into Oracle Corporation) </p>\r\n<p>Initial Name : OAK language </p>\r\n<p>Present Name : java </p>\r\n<p>Initial version : jdk 1.0 (java development kit)</p> \r\n<p>Present version : java 8 2014 </p>\r\n<p>Type : open source & free software </p>\r\n<p>Extensions : .java & .class & .jar</p> \r\n<p>Operating System : multi Operating System </p>\r\n<p>Implementation Lang : c, cppâ€¦â€¦ </p>\r\n<p>Symbol : coffee cup with saucer </p>\r\n<p>SUN : Stanford Universally Network</p> \r\n<p>Slogan/Motto : WORA(write once run anywhere) </p>\r\n<p>Compilation : java compiler </p>\r\n<p>Execution : JVM(java virtual machine) </p>\r\n'),
(4, 'JAVA Features ( Buzz words) ', '<p><b>1. Simple </b></p>\r\n<p>Java technology has eliminated all the difficult and confusion oriented concepts like pointers, multiple inheritance in the java language. </p>\r\n<p>Java uses c,cpp syntaxes mainly hence who knows C,CPP for that java is simple language. </p>\r\n<p><b>2. Object Oriented </b></p>\r\n<p>Java is object oriented because it is representing total data of the class in the form of object. </p>\r\n<p>The languages which are support object,class,Inheritance,Polymorphism, \r\nEncapsulation,Abstraction those languages are called Object oriented. </p>\r\n<p><b>3. Robust </b></p>\r\n<p>Any technology good at two main areas that technology is robust technology. </p>\r\n<p>a. Exception Handling </p>\r\n<p>b. Memory Allocation </p>\r\n<p>Java is providing predefined support to handle the exceptions. </p>\r\n<p>Java provides Garbage collector to support memory management. </p>\r\n<p><b>4. Architectural Neutral </b></p>\r\n<p>Java applications are compiled in one Architecture/hardware (RAM, Hard Disk) and that Compiled program runs on any architecture(hardware) is called Architectural Neutral.</p> \r\n<p><b>5. Platform Independent</b></p>\r\n<p>Once we develop the application by using any one operating system(windows) that application runs only on same operating system is called platform dependency. </p>\r\n<p>Ex :- C,CPP </p>\r\n<p>Once we develop the application by using any one operating system(windows) that application runs on in all operating system is called platform independency. </p>\r\n<p>Ex :- java </p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(10) NOT NULL,
  `lan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `lan`) VALUES
(1, 'java'),
(2, 'php'),
(3, 'python');

-- --------------------------------------------------------

--
-- Table structure for table `php`
--

CREATE TABLE `php` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `data` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `php`
--

INSERT INTO `php` (`id`, `title`, `data`) VALUES
(1, 'What is PHP', 'PHP is a open source, interpreted and object-oriented scripting language i.e. executed at server side. It is used to develop web applications (an application i.e. executed at server side and generates dynamic page).\r\n<p>PHP stands for Hypertext Preprocessor.</p>\r\n<p>PHP is an interpreted language, i.e., there is no need for compilation.</p>\r\n<p>PHP is a server-side scripting language.</p>\r\n<p>PHP is faster than other scripting languages, for example, ASP and JSP.</p>\r\n<p>PHP is simple and easy to learn language.</p>'),
(2, 'PHP Features', '<p><b>There are given many features of PHP.</b></p>\r\n<p><b>Performance:</b> Script written in PHP executes much faster then those scripts written in other languages such as JSP & ASP.</p>\r\n<p><b>Open Source Software:</b> PHP source code is free available on the web, you can developed all the version of PHP according to your requirement without paying any cost.</p>\r\n<p><b>Platform Independent:</b> PHP are available for WINDOWS, MAC, LINUX & UNIX operating system. A PHP application developed in one OS can be easily executed in other OS also.</p>\r\n<p><b>Compatibility:</b> PHP is compatible with almost all local servers used today like Apache, IIS etc.</p>\r\n<p><b>Embedded:</b> PHP code can be easily embedded within HTML tags and script.</p>\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `python`
--

CREATE TABLE `python` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `data` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(10) NOT NULL,
  `lan` varchar(20) NOT NULL,
  `ques_no` int(10) NOT NULL,
  `question` varchar(500) NOT NULL,
  `option1` varchar(500) NOT NULL,
  `option2` varchar(500) NOT NULL,
  `option3` varchar(500) NOT NULL,
  `option4` varchar(500) NOT NULL,
  `ans` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `lan`, `ques_no`, `question`, `option1`, `option2`, `option3`, `option4`, `ans`) VALUES
(1, 'java', 1, 'Which of the following option leads to the portability and security of Java?', 'Bytecode is executed by JVM', 'The applet makes the Java code secure and portable', 'Use of exception handling', 'Dynamic binding between objects', 'a'),
(2, 'java', 2, 'Which of the following is not a Java features?', 'Dynamic', 'Architecture Neutral', 'Use of pointers', 'Object-oriented', 'c'),
(3, 'java', 3, ' The u0021 article referred to as a', 'Unicode escape sequence', 'Octal escape', 'Hexadecimal', 'Line feed', 'a'),
(4, 'java', 4, '_____ is used to find and fix bugs in the Java programs.', 'JVM', 'JRE', 'JDK', 'JDB', 'd'),
(5, 'java', 5, 'What is the return type of the hashCode() method in the Object class?', 'Object', 'int', 'long', 'void', 'b'),
(6, 'java', 6, 'Which of the following is a valid long literal?', 'ABH8097', 'L990023', '904423', '0xnf029L', 'd'),
(7, 'java', 7, 'What does the expression float a = 35 / 0 return?', '0', 'Not a Number', 'Infinity', 'Run time exception', 'c'),
(8, 'java', 8, 'Which of the following tool is used to generate API documentation in HTML format from doc comments in source code?', 'javap tool', 'javaw command', 'Javadoc tool', 'javah command', 'c'),
(9, 'java', 9, 'Which of the following creates a List of 3 visible items and multiple selections abled?', 'new List(false, 3)', 'new List(3, true)', 'new List(true, 3)', 'new List(3, false)', 'b'),
(10, 'java', 10, 'Which of the following for loop declaration is not valid?', 'for ( int i = 99; i >= 0; i / 9 )', 'for ( int i = 7; i <= 77; i += 7 )', 'for ( int i = 20; i >= 2; - -i )', 'for ( int i = 2; i <= 20; i = 2* i )', 'a'),
(11, 'php', 1, 'PHP Stands for?', 'PHP Hypertex Processor', 'PHP Hyper Markup Processor', 'PHP Hyper Markup Preprocessor', 'PHP Hypertext Preprocessor', 'd'),
(12, 'php', 2, '	\r\nPHP is an example of ___________ scripting language.', 'Server-side', 'Client-side', 'Browser-side', 'In-side', 'a'),
(13, 'php', 3, 'Who is known as the father of PHP?', 'Rasmus Lerdorf', 'Willam Makepiece', 'Drek Kolkevi', 'List Barely', 'a'),
(14, 'php', 4, 'Which of the following is not true?', 'PHP can be used to develop web applications.', 'PHP makes a website dynamic', 'PHP applications can not be compile', 'PHP can not be embedded into html.', 'd'),
(15, 'php', 5, 'PHP is ___ typed language', 'Loosely', 'Server', 'User', 'System', 'a'),
(16, 'php', 6, 'Which of the following variables is not a predefined variable?', '$get', '$ask', '$request', '$post', 'b'),
(17, 'php', 7, 'When you need to obtain the ASCII value of a character which of the following function you apply in PHP?', 'chr( );', 'asc( );', 'ord( );', 'val( );', 'c'),
(18, 'php', 8, 'Which of the following method sends input to a script via a URL?', 'Get', 'Post', 'Both', 'None', 'a'),
(19, 'php', 9, 'Which of the following function returns a text in title case from a variable?', 'ucwords($var)', 'upper($var)', 'toupper($var)', 'ucword($var)', 'a'),
(20, 'php', 10, 'Which of the following function returns the number of characters in a string variable?', 'count($variable)', 'len($variable)', 'strcount($variable)', 'strlen($variable)', 'd'),
(21, 'java', 0, 'What is the use of the intern() method?', 'It returns the existing string from memory', 'It creates a new string in the database', 'It modifies the existing string in the database', 'None of the above', 'a'),
(22, 'java', 0, 'What is the use of the intern() method?', 'It returns the existing string from memory', 'It creates a new string in the database', 'It modifies the existing string in the database', 'None of the above', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `psw` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `psw`, `email`, `mobile`) VALUES
(1, 'srujana', 'srujana', 'srujana@gmail.com', '7036347126');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `java`
--
ALTER TABLE `java`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `php`
--
ALTER TABLE `php`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `python`
--
ALTER TABLE `python`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `java`
--
ALTER TABLE `java`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `php`
--
ALTER TABLE `php`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `python`
--
ALTER TABLE `python`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
